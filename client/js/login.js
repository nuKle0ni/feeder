/* Disables the 3rd party logins
when the configuration is not finished
isLoginSelected updates the text in the log in button
*/

Template.login.helpers({
  "isDisabled": function() {
    if (!Accounts.loginServicesConfigured()) {
      return "disabled";
    } else {
      return null;
    }
  },
  "isLoginSelected": function() {
    if (Session.get("isLoginSelected")) {
      return "checked";
    } else {
      return null;
    }

  }
});

/*guide here http://meteortips.com/second-meteor-tutorial/user-accounts/*/

Template.login.onRendered(function() {});

Template.login.events({
  /*toggle the text when radio selection changes*/
  "change #registerradio": function() {
    Session.set("isLoginSelected", false);
  },
  "change #loginradio": function() {
    Session.set("isLoginSelected", true);
  },
  /*Same button handles log in and register
  Decide whether to Meteor.login or register
  and show errors if needed with semantic popup */
  "click .submitbtn": function() {
    var email = $('#email').val();
    var pw = $('#password').val();

    if (Session.get("isLoginSelected")) {
      Meteor.loginWithPassword(email, pw, function(error) {
        if (error) {
          $(".submitbtn").popup({
            "content": error.reason,
            on: "manual",
            position: "top right"
          });
          $('.submitbtn').popup("show");
          setTimeout(function() {
            $('.submitbtn').popup("hide")
          }, 3000);
          return;
        }
        $('.submitbtn').popup('hide');
        return Router.go("/");
      });
    } else {
      Accounts.createUser({
        email: email,
        password: pw
      }, function(error) {
        if (error) {
          $(".submitbtn").popup({
            "content": error.reason,
            on: "manual",
            position: "top right"
          });
          $('.submitbtn').popup("show");
          setTimeout(function() {
            $('.submitbtn').popup("hide")
          }, 3000);
          return;
        } else {
          $('.submitbtn').popup('hide');
          Session.set("recipeQueue", []);
          return Router.go("/");
        }
      });
    }
  },
  /*Login with meteors build in 3rd party functions*/
  "click .googlebtn": function() {
    Meteor.loginWithGoogle({
        requestPermissions: ['email']
      },
      function(error) {
        if (error) {
          return;
        } else {
          $('.submitbtn').popup('hide');
          Session.set("recipeQueue", []);
          return Router.go("/");
        }
      });
  },
  "click .facebookbtn": function() {
    Meteor.loginWithFacebook({
        requestPermissions: ['email']
      },
      function(error) {
        if (error) {
          return;
        } else {
          $('.submitbtn').popup('hide');
          Session.set("recipeQueue", []);
          return Router.go("/");
        }
      });
  }
});
