Template.foodmenu.onRendered(function() {
  $('.ui.accordion').accordion();
});

Template.foodmenu.helpers({
  recipes: function() {
    return Meteor.user().profile.weeksmenu;
  }
});

Template.foodmenu.events({});

Template.shoppinglist.helpers({
  groceries: function() {
    return Meteor.user().profile.shoppinglist;
  }
});
