/* attach jquery when done rendering (same as $document ready
but that cant be used with meteor)*/
Template.foodcard.onRendered(function() {
  $('.foodcard').dimmer();
});

/*helpers return data to html templates (can be used with eg. {{recipe}} )*/
Template.foodcard.helpers({
  recipe: function() {
    return Session.get("recipe");
  },
  image: function() {
    return Session.get("recipe").images[0].hostedLargeUrl;
  },
  title: function() {
    return Session.get("recipe").name;
  },
  ingredients: function() {
    return Session.get("recipe").ingredientLines;
  }
});

/* events get called when something happens inside template */
Template.foodcard.events({
  /*show ingredients inside card*/
  "click .foodimg": function() {
    $('.foodcard').dimmer("toggle");
  },
  "click .dimmer": function() {
    $('.foodcard').dimmer("toggle");
  },
  /*start getting food ideas, get last one from recipe queue
  and call getNewRecipe to get detailed info about that recipe */
  "click .startbtn": function() {
    var recipeQueue = Session.get("recipeQueue");
    var recipe = recipeQueue[recipeQueue.length - 1];
    var recipeID = recipe.id;
    recipeQueue.pop();
    Session.set("recipeQueue", recipeQueue);
    getNewRecipe(recipeID);

  },
  /*Just update the recipequeue and get new recipe*/
  "click .dislikebtn": function() {
    $('.foodcard').transition('fade right');
    var recipeQueue = Session.get("recipeQueue");
    var recipe = recipeQueue[recipeQueue.length - 1];
    Session.set("toShoppingList", recipe.ingredients);
    recipeQueue.pop();
    Session.set("recipeQueue", recipeQueue);
    getNewRecipe(recipe.id);
  },
    /* Update users profile for both menu and ingredients
    then update the queue and get new recipe.*/
  "click .likebtn": function() {
    $('.foodcard').transition('fade left');
    Meteor.users.update(Meteor.userId(), {
      $push: {
        "profile.weeksmenu": Session.get("recipe")
      }
    });
    if (Session.get("toShoppingList") != null) {
      Meteor.users.update(Meteor.userId(), {
        $addToSet: {
          "profile.shoppinglist": {
            $each: Session.get("toShoppingList")
          }
        }
      });
    }
    var recipeQueue = Session.get("recipeQueue");
    var recipe = recipeQueue[recipeQueue.length - 1];
    Session.set("toShoppingList", recipe.ingredients);
    recipeQueue.pop();
    Session.set("recipeQueue", recipeQueue);
    getNewRecipe(recipe.id);
  }
});

/* Calls the server for new recipe and sets it in the Session */
function getNewRecipe(recipeID) {
  Meteor.call("getRecipe", recipeID, function(error, resultJson) {
    if (error) {
    } else {

      Session.set("recipe", resultJson);
    }
    $('.foodcard').transition('scale');
  });
}
