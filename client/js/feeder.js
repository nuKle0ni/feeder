
/* init values */
Session.setDefault("isLoginSelected", true);
Session.setDefault("recipeQueue", null);
Session.setDefault("recipe", null);

/*default values to show as title and meta description if
no seo plugin info was found */
var defaults = {
  title: 'Feeder',
  suffix: 'Feeder',

  description: 'Feeder is a Tinder-like food recommendation app',

  meta: {
    keywords: ['feeder', 'meteor', 'food', 'recommendation']
  }
};
/* Attach router with seo plugin */
Router.plugin('seo', {
  "default": defaults
});

/*Tracker autorun gets automaticallu called again every
time it's dependency change. In this
case Meteor.user() changes or Session recipequeue.
So we get a new recipequeue everytime it gets emptied*/
Tracker.autorun(function() {
  if (Meteor.user()) {
    var recipeQueue = Session.get("recipeQueue");
    if (recipeQueue == null || recipeQueue.length == 0) {
      Meteor.call("searchRecipes", null, function(error, resultJson) {
        Session.set("recipeQueue", resultJson);
      });
    }
  }
});


/* Logs user out and routes to login screen */
Template.navbar.events({
  "click .logoutbtn": function() {
    Router.go("/login");
    Meteor.logout();
  }
});
