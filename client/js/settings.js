/*init the confirmation popup for clearing menu and shopping list
Set the value of the diet dropdown to current users diet
*/
Template.settings.onRendered(function() {
  $('.ui.modal').modal({
    onApprove: okClicked()
  });
  $('.diet option[value="' + Meteor.user().profile.diet + '"]').attr('selected', true);
});

/* update the diet when selection in the dropdown changes and load new
recipequeue by setting recipequeue to null (autorun)*/
Template.settings.events({
  "change .diet": function(evt) {
    Meteor.users.update(Meteor.userId(), {
      $set: {
        "profile.diet": $(evt.target).val()
      }
    });
    Session.set("recipeQueue", null);
  },
  /*prompt user to confirm the clear action*/
  "click .clearbtn": function() {
    $('.ui.modal').modal('show');
  }
});

/*called by semantic ui when user clicks ok buttton in the modal
clear all users lists*/
function okClicked() {
  Meteor.users.update(Meteor.userId(), {
    $set: {
      "profile.weeksmenu": []
    }
  });
  Meteor.users.update(Meteor.userId(), {
    $set: {
      "profile.shoppinglist": []
    }
  });
}
