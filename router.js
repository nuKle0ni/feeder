/* Router handles navigation in the app.
It updates the url. It renders the changing
content in the layoutTemplate slot (in html <template layout> {{>yield}}
It also has SEO plugin which updates the metadata on every page
It checks also if the user is logged in and redirects if needed.
*/

Router.configure({
  layoutTemplate: "layout"
});

Router.route("/", {
    template:"home",
    seo: {
      title:"Feeder - Home",
      description: "Feeder homepage"
    }
});

Router.route("/login",{
  seo: {
    title:"Feeder - Log in page",
    description: "Feeder log in page"
  }
});

Router.route("/cards",{
    template: "foodcard",
    seo: {
      title:"Feeder - Ideas",
      description: "Feeder Ideas"
    }
});

Router.route("/weeksmenu",{
    template: "foodmenu",
    seo: {
      title:"Feeder - Week's menu",
      description: "Feeder week's menu"
    }
});

Router.route("/shoppinglist", {
  seo: {
        title:"Feeder - Shopping list",
        description: "Feeder shopping list"
      }
});

Router.route("/settings", {
  template:"settings",
  layoutTemplate: "layout",
  seo: {
        title:"Feeder - Settings",
        description: "Feeder Settings"
      }
});

/*check loggedin on every page except itself*/
Router.onBeforeAction(function () {
  if (!Meteor.userId()) {
    this.render('Login');
  } else {
    this.next();
  }
}, {
  except:["login"]
});
