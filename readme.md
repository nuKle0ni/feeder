Feeder is a Tinder-like application for meal recommendations.

User swipes right to like a recipe and left to discard.

Liked recipes get added to user's menu and grocery list.

Recipes are provided by Yummly.

Built with Meteor.
