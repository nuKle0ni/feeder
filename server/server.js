
/* total search results. Used when randoming the search */
var totalMatches = 0;

/* http://joshowens.me/environment-settings-and-security-with-meteor-js/ */

/* Methods can be called by the client with Meteor.call */
Meteor.methods({
  /* get new recipe to be used in card */
  getRecipe: function(recipeID) {
    var url = "http://api.yummly.com/v1/api/recipe/" + recipeID;
    var options = {
      "params": {
        "_app_id": Meteor.settings.yummlyapp,
        "_app_key": Meteor.settings.yummlykey,
      }
    };
    var result = HTTP.get(url, options);
    var json = JSON.parse(result.content);
    return json;
  },
  /* call yummly api with random search selection. Acts as recipequeue in client */
  searchRecipes: function() {
    var url = "http://api.yummly.com/v1/api/recipes";
    var options = {
      "query": "easy&requirePictures=true",
      "params": {
        "_app_id": Meteor.settings.yummlyapp,
        "_app_key": Meteor.settings.yummlykey,
        "allowedCourse[]":"course^course-Main Dishes",
        "allowedDiet[]": diet(),
        "maxResult": 10,
        "start": Math.floor((Math.random() * totalMatches) + 1)
      }
    };
    var result = HTTP.get(url, options);
    var json = JSON.parse(result.content);
    totalMatches = json.totalMatchCount;
    return json.matches;
  }
});

function diet() {
  try {
    var result = Meteor.user().profile.diet;
    return result;
  }
  catch (err) {
    return "";
  }
}
