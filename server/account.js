/* Setup 3rd party services */

ServiceConfiguration.configurations.remove({
  service: "google"
});

ServiceConfiguration.configurations.remove({
  service: "facebook"
});
ServiceConfiguration.configurations.insert({
  service: "google",
  loginStyle: "popup",
  clientId: Meteor.settings.googleClientId,
  secret: Meteor.settings.googleSecret
});

ServiceConfiguration.configurations.insert({
    service: 'facebook',
    appId: Meteor.settings.facebookAppId,
    secret: Meteor.settings.facebookSecret
});
